const chai = require("chai");
const expect = chai.expect;
const http = require("chai-http");
chai.use(http);

describe("forex_api_test_suite", () => {
  it("test_api_get_rates_is_running", () => {
    chai
      .request("http://localhost:5001")
      .get("/rates")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
      });
  });

  it("test_api_get_rates_returns_200", (done) => {
    chai
      .request("http://localhost:5001")
      .get("/rates")
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  it("test_api_get_rates_returns_object_of_size_5", (done) => {
    chai
      .request("http://localhost:5001")
      .get("/rates")
      .end((err, res) => {
        expect(Object.keys(res.body.rates)).does.have.length(5);
        done();
      });
  });
});

describe("forex_api_test_suite_currency", () => {
  it("test_api_post_currency_is_running", () => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
      });
  });

  it("test_api_post_currency_returns_400_if_no_name", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        alias: "aud",
        ex: {
          peso: 36.58,
          usd: 0.64,
          won: 851.25,
          yuan: 4.69,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("test_api_post_currency_returns_400_if_name_is_not_string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        alias: "aud",
        name: 1.0,
        ex: {
          peso: 36.58,
          usd: 0.64,
          won: 851.25,
          yuan: 4.69,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("test_api_post_currency_returns_400_if_name_is_empty", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        alias: "aud",
        name: "",
        ex: {
          peso: 36.58,
          usd: 0.64,
          won: 851.25,
          yuan: 4.69,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("test_api_post_currency_returns_400_if_no_ex", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        alias: "aud",
        name: "Australian Dollar",
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("test_api_post_currency_returns_400_if_ex_is_not_object", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        alias: "aud",
        name: "Australian Dollar",
        ex: 1.0,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("test_api_post_currency_returns_400_if_ex_is_empty", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        alias: "aud",
        name: "Australian Dollar",
        ex: {},
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("test_api_post_currency_returns_400_if_no_alias", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        name: "Australian Dollar",
        ex: {
          peso: 36.58,
          usd: 0.64,
          won: 851.25,
          yuan: 4.69,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("test_api_post_currency_returns_400_if_alias_is_not_string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        alias: 1.0,
        name: "Australian Dollar",
        ex: {
          peso: 36.58,
          usd: 0.64,
          won: 851.25,
          yuan: 4.69,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("test_api_post_currency_returns_400_if_alias_is_empty", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        alias: "",
        name: "Australian Dollar",
        ex: {
          peso: 36.58,
          usd: 0.64,
          won: 851.25,
          yuan: 4.69,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("test_api_post_currency_returns_400_if_complete_fields_but_duplicate_alias", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        alias: "peso",
        name: "Mexican Peso",
        ex: {
          peso: 3.37,
          usd: 0.059,
          won: 78.41,
          yuan: 0.43,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("test_api_post_currency_returns_200_if_complete_fields_no_duplicates", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .type("json")
      .send({
        alias: "aud",
        name: "Australian Dollar",
        ex: {
          peso: 36.58,
          usd: 0.64,
          won: 851.25,
          yuan: 4.69,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });
});
